import org.jsoup.Jsoup;
import org.jsoup.helper.ChangeNotifyingArrayList;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.jsoup.nodes.Element;
import java.io.IOException;
import java.util.ArrayList;

public class App {

    Page master;

    public static void main(String[] args) {
        String url = "https://en.wikipedia.org/wiki/Wikipedia";
        Page parent = new Page(url, 0);
        parent.initFileReader();
        try {
            parent.activate();
            ArrayList<ArrayList<Page>> children = parent.getGens();
            for (int i = 1; i < children.size(); i++) {
                for (int j = 0; j < children.get(i).size(); j++) {
                    children.get(i).get(j).findPages();
                }
            }
            //made it to 2057
        } catch (Exception e) {
            e.printStackTrace();
        }
        parent.tree(0);
        parent.closeFileReader();
    }
}
