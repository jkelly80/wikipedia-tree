import java.io.FileWriter;
import java.util.ArrayList;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import java.io.File;

import java.io.IOException;


public class Page {

    private static final String baseUrl = "https://en.wikipedia.org";
    private static String file = "/Users/JackKelly/Desktop/output.txt";
    private String title;
    private int generation;
    private String url;
    private ArrayList<Page> pages = new ArrayList<Page>();
    private boolean isSterile = false;
    private static ArrayList<ArrayList<Page>> gens = new ArrayList<ArrayList<Page>>();
    public static int count = 0;
    private static FileWriter karel;
    public static int uniqueCount = 0;
    private static String[] noNoWords = {".org", ".svg", ".png", "Wikipedia:"
            , ".jpg", "talk:", "Portal:", "Special", "RecentChanges", "Help:"
            , "Talk:", "File", "(disambiguation)", "Category:"};

    public Page(String bravo, int charlie) {
        url = bravo;
        generation = charlie;
        count++;
        if ((count % 100) == 0) {
            System.out.println("We have made " + count + " pages (" + uniqueCount + " " +
                    "unique) " +
                    "with current " +
                    "generation: " + generation + ".");
        }
        if (generation >= gens.size()) {
            gens.add(new ArrayList<Page>());
        }
        Document doc = null;
        try {
            doc = Jsoup.connect(url).timeout(0).get();
            String temp = doc.title();
            title = temp.substring(0, temp.length() - 12);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void initFileReader() {
        try {
            karel = new FileWriter(new File(file), true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void activate() {
        try {
            findPages();
        } catch (Exception e) {
            e.printStackTrace();
        }
        for (int i = 0; i < pages.size(); i++) {
            try {
                pages.get(i).findPages();
            } catch (Exception e) {
                continue;
            }
        }
    }

    public ArrayList<ArrayList<Page>> getGens() {
        return gens;
    }


    public String getTitle() {
        return title;
    }

    public String getUrl() {
        return url;
    }

    public int getGeneration() {
        return generation;
    }

    @Override
    public boolean equals(Object other) {
        if (this ==  other) {
            return true;
        }else if (other instanceof Page) {
            return this.title.equals(((Page) other).title);
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return title.hashCode();
    }


    public void findPages() throws Exception {
        if (!isSterile) {
            Document doc = Jsoup.connect(url).timeout(0).get();
            Element body = doc.body();
            Elements elements = body.getElementsByAttribute("href");
            for (int i = 0; i < 10; i++) { //TODO elements.size()
                String attributes = elements.get(i).attributes().toString();
                if (attributes.contains("/wiki/") && !hasMatches(attributes)) {
                    int lastIndex = attributes.indexOf("\"", 7);
                    String newUrl = baseUrl + attributes.substring(7, lastIndex);
                    if (count < 1000) {
                        Page temp = new Page(newUrl, generation + 1);
                        if (WikiUtils.exists(temp)) {
                            temp.isSterile = true;
                        } else {
                            WikiUtils.addToArray(temp);
                            uniqueCount++;
                        }
                        pages.add(temp);
                        gens.get(generation).add(temp);
                        temp = null;
                    }
                }
                attributes = null;
            }
            body = null;
            elements = null;
            System.gc();
        }

    }

    private boolean hasMatches(String alpha) {
        for (int i = 0; i < noNoWords.length; i++) {
            if (alpha.contains(noNoWords[i])) {
                return true;
            }
        }
        return false;
    }

    public static void closeFileReader() {
        try {
            karel.flush();
            karel.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void tree(int whiteSpace) {
        System.out.println("Making tree");

        try {
            if (karel == null) {
                initFileReader();
            }
            karel.write(this.title + "\n");
            //System.out.println(this.title);
            for (int i = 0; i < pages.size(); i++) {
                karel.write(addWhiteSpace(whiteSpace));
                karel.write("│\n");
                //System.out.println("│");
                karel.write(addWhiteSpace(whiteSpace));
                //System.out.print("└─ ");
                karel.write("└─ ");
                pages.get(i).tree(whiteSpace + 1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String addWhiteSpace(int whiteSpace) {
        String str = "";
        for (int i = 0; i < whiteSpace; i ++) {
            str += "│ ";
            //System.out.print("│ ");
        }
        return str;
    }

}
