import java.util.ArrayList;
import java.util.HashMap;

public class WikiUtils {

    private static HashMap<Integer, Page> pages = new HashMap<Integer, Page>();

    public static void addToArray(Page alpha) {
        int hash = alpha.hashCode();
        pages.put(hash, alpha);
    }

    public static boolean exists(Page alpha) {
        int hash = alpha.hashCode();
        return pages.containsKey(hash);
    }
}
